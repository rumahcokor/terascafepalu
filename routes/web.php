<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\customerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function (){
    return view('welcome');
});
Route::get('/customer', [customerController::class , 'customer'] );
Route::post('/inputcustomer' , [customerController::class , 'input']);
Route::get('/admin' , [customerController::class , 'admin']);
Route::post('/adminlogin' , [customerController::class , 'loginadmin']);
Route::post('/customersearch/' , [customerController::class ,'searchcustomer']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

