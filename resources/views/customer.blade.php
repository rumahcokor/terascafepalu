<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Teras Cafe</title>
    <meta name="description" content="Menyajikan sepenuh hati dengan kuliatas terbaiknya. Ayo mampir ke Teras Cafe untuk merasakan sensasi makanan khas jepang yang dibuat sepenuh hati, dengan tangan professional kami akan Menyajikan cita rasa nikmat dan lezat">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

</head>
<body>

<h1 class="m-5 text-center">Id Customer</h1>



<div class="container">
    <form class="m-5" method="post" action="/customersearch">
        @csrf
        @method("POST")
        <div class="row">
            <div class="col-3">
                <label for="inputPassword6" class="col-form-label">Input Nama Customer</label>
            </div>
            <div class="col-auto">
                <input type="text" name="nama" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline">
            </div>
            <div class="col-3">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>


        </div>
    </form>
    <table class="table table-success table-striped">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
        </tr>

        @foreach($data as $datas)
            <tr>
                <td>{{$datas->id}}</td>
                <td>{{$datas->nama}}</td>
                <td>{{$datas->email}}</td>
            </tr>
        @endforeach


        </thead>
    </table>
    <a href="/" class="btn btn-danger m-2">Back</a>
    <a href="/customer" class="btn btn-primary m-2">Refresh</a>
    <h3>{{$data->links()}}</h3>


</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


</body>
</html>
