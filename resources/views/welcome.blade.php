<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Tastebite">
    <meta name="keywords" content="Tastebite">
    <meta name="author" content="Tastebite">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Teras Cafe - Spesialnya Mie Ramen</title>
    <meta name="description" content="Menyajikan sepenuh hati dengan kuliatas terbaiknya. Ayo mampir ke Teras Cafe untuk merasakan sensasi makanan khas jepang yang dibuat sepenuh hati, dengan tangan professional kami akan Menyajikan cita rasa nikmat dan lezat">

    <!-- Tastebite External CSS -->
    <link href="{{asset('assets/css/owl.carousel.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('assets/css/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('assets/css/tastebite-styles.css')}}" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="{{asset('/assets/css/index.css')}}">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <style>
        body{

            background-image: url("{{asset('background/back3.png')}}");
            background-size: 1080px;
            background-repeat: repeat;



        }
        *{
            margin: 0;
            padding: 0;
        }

        .stroke{
            -webkit-text-stroke-width: 2px;
            -webkit-text-stroke-color: black;
        }

        .strokek{
            -webkit-text-stroke-width: 1px;
            -webkit-text-stroke-color: black;
        }

        .judul{
            font-family: 'Bungee', cursive;

        }

        .kuning{
            color: #FFC402;
        }

        .isi{
            font-family: 'Open Sans', sans-serif;
        }

        .bekgrn{
            background-color: #FFC402;
        }

        .text-shadow{
            text-shadow: 2px 2px 4px #000000;
        }

        .bekgrn_merah {
            background-color: #EA2229
        }


    </style>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bungee&family=Open+Sans&display=swap" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light m-0 sticky-top text-white" style="background-color: #EA2229">
    <!--tips: to change the nav placement use .fixed-top,.fixed-bottom,.sticky-top-->
    <div class="container">
        <a class="navbar-brand text-white judul strokek text-shadow" href="/">
            <img src="assets/images1/logo_teras.png" alt="logo teras cafe" style=" width: 120px;" > Teras Cafe
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>




        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" style="color: black;" href="#menuus">Tentang Kami </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" style="color: black;" href="#menuuu">Daftar Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: black;" href="/admin">Admin</a>
                </li>
            </ul>
        </div>
    </div>
    </nav>






    <div data-aos="zoom-out">
        <div class="art bekgrn rounded shadow m-3 p-1">
            <div class="row pt-md-5 mt-md-4">
                <div class="col-lg-6 order-2 order-md-0 img-fluid gmbr shadow-24" data-aos="fade-right">
                    <img src="assets/images/menus/back.png" class="w-100 mb-3" alt="Menu">
                </div>
                <div class="col-lg-6 p-5 shadow" data-aos="fade-left" style="background: #EA2229">
                    <h6 class="display-4 text-white stroke text-shadow" style="font-family: 'Bungee', cursive;">Teras Cafe <br> <div style="color:#FFC402 ">Spesialnya Mie Ramen</div></h6>
                    <p class="display-8 text-white isi">Menyajikan sepenuh hati dengan kuliatas terbaiknya. Ayo mampir ke Teras Cafe untuk merasakan sensasi makanan khas jepang yang dibuat sepenuh hati, dengan tangan professional kami akan Menyajikan cita rasa nikmat dan lezat</p>
                    <a name="" class="btn btn-outline-secondary"  href="#desc" role="button">Kunjungi Sekarang</a>
                </div>
            </div>
        </div>
    </div>




    <div class="middle  container-fluid p-5 text-white"  style="text-align: center; background: #EA2229 ">
        <div data-aos="zoom-in">
            <h6 class="display-1 judul stroke text-shadow">Teras Cafe</h6>
            <p class="display-4 judul kuning stroke text-shadow">The 1st Ramen In Palu City </p>
        </div>
    </div>


<div class="container-md">
    <div class=" rounded shadow ml-0 " style="background-color: #EA2229">
        <section class="tstbite-components my-4">
            <h4 class="shadow-24 text-white rounded-6 p-3 judul strokek bekgrn text-shadow " >Recommended Menu</h4>
            <div data-aos="zoom-in-down">
                <div class="row p-5" id="high1">

                    <div class="col-xl-4 col-md-6 ">
                        <figure class="my-3 tstbite-card">
                            <a href="#0" class="tstbite-animation rounded-top-6">
                                <img src="assets/images/Teras_Menus/33.jpg" style="max-height:259.69px " class="w-100 recom" alt="Ramen Goreng">
                            </a>
                            <figcaption class="border-top-0 rounded-bottom-6 bekgrn">
                                <div class="text-black pt-3 pb-4 px-4">
                                    <div class="w-100 float-start">
                                        <div class="float-start">
                                            <div class="fabrx-ratings has-rating rating">
                                                <input type="radio" id="radio1" name="rate1" value="1" checked="checked">
                                                <label for="radio1" class="custom-starboxes"></label>
                                                <input type="radio" id="radio2" name="rate1" value="2">
                                                <label for="radio2" class="custom-starboxes"></label>
                                                <input type="radio" id="radio3" name="rate1" value="3">
                                                <label for="radio3" class="custom-starboxes"></label>
                                                <input type="radio" id="radio4" name="rate1" value="4">
                                                <label for="radio4" class="custom-starboxes"></label>
                                                <input type="radio" id="radio5" name="rate1" value="5">
                                                <label for="radio5" class="custom-starboxes"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h6 class="judul f-size-20 mb-0 font-weight-semibold"><a href="#0" class="text-black">Ramen Goreng Katsu</a></h6>
                                    <div class="mt-3">
                                        <p class="isi">Ramen Goreng Dengan Potongan Ayam Goreng Atau Katsu</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-xl-4 col-md-6 ">
                        <figure class="my-3 tstbite-card bekgrn">
                            <a href="#0" class="tstbite-animation rounded-top-6">
                                <img src="assets/images/Teras_Menus/Shitake.JPG" class="w-100" alt="Ramen Shitake">
                            </a>
                            <figcaption class="border-top-0 rounded-bottom-6 ">
                                <div class="text-black pt-3 pb-4 px-4">
                                    <div class="w-100 float-start">
                                        <div class="float-start">
                                            <div class="fabrx-ratings has-rating rating">
                                                <input type="radio" id="radio6" name="rate2" value="1" checked="checked">
                                                <label for="radio6" class="custom-starboxes"></label>
                                                <input type="radio" id="radio7" name="rate2" value="2">
                                                <label for="radio7" class="custom-starboxes"></label>
                                                <input type="radio" id="radio8" name="rate2" value="3">
                                                <label for="radio8" class="custom-starboxes"></label>
                                                <input type="radio" id="radio9" name="rate2" value="4">
                                                <label for="radio9" class="custom-starboxes"></label>
                                                <input type="radio" id="radio10" name="rate2" value="5">
                                                <label for="radio10" class="custom-starboxes"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h6 class="judul f-size-20 mb-0 font-weight-semibold"><a href="#0" class="text-black">Ramen Jamur Shitake</a></h6>
                                    <div class="mt-3">
                                        <p>Ramen Kaldu Ayam Topping Jamur Shitake & Ayam br Kecap </p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xl-4 col-md-6 ">
                        <figure class="my-3 tstbite-card bekgrn">
                            <a href="#0" class="tstbite-animation rounded-top-6">
                                <img src="assets/images/Teras_Menus/Miruku.JPG" class="w-100" alt="Ramen Spicy Miruku">
                            </a>
                            <figcaption class="border-top-0 rounded-bottom-6 bekgrn">
                                <div class="text-black pt-3 pb-4 px-4">
                                    <div class="w-100 float-start">
                                        <div class="float-start">
                                            <div class="fabrx-ratings has-rating rating">
                                                <input type="radio" id="radio11" name="rate3" value="1" checked="checked">
                                                <label for="radio11" class="custom-starboxes"></label>
                                                <input type="radio" id="radio12" name="rate3" value="2">
                                                <label for="radio12" class="custom-starboxes"></label>
                                                <input type="radio" id="radio13" name="rate3" value="3">
                                                <label for="radio13" class="custom-starboxes"></label>
                                                <input type="radio" id="radio14" name="rate3" value="4">
                                                <label for="radio14" class="custom-starboxes"></label>
                                                <input type="radio" id="radio15" name="rate3" value="5">
                                                <label for="radio15" class="custom-starboxes"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h6 class="judul f-size-20 mb-0 font-weight-semibold"><a href="#0" class="text-black">Ramen Spicy Miruku</a></h6>
                                    <div class="mt-3">

                                        <p>Ramen Kaldu Ayam Susu Dengan Topping Ayam Bakar Pedas</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<a href="#opening"> <img src="{{asset('/background/1x/Artboard 1.png')}}" class="w-100 img-fluid" style="margin-bottom: 2rem;" alt="Banner Promo Teras Cafe">
</a>

<div class="container">




    <div data-aos="zoom-in-down">
        <div class="shadow row bekgrn rounded" >
            <div class="col-lg-6 text-white bekgrn_merah" style="">
                <h4 class="display-3 judul stroke text-shadow " style="text-align: center;margin-bottom: 10px">Dibuat Dengan <div class="kuning"> Kualitas </div></h4>
                <p>Kami Menggunakan bahan makanan yang berkulitas, serta memperhatikan dan memastikan setiap bahan agar tetap segar </p><br>
                <p>Dimasak dan diolah dengan tangan professional untuk menjaga kuliatas dari rasa Teras Cafe, serta disajikan dengan tepat sesuai permintaan</p>
            </div>
            <div class="col-lg-6 p-4" id="sushi">
                <img src="assets/images/Teras_Menus/Sushi.JPG" class="w-100" alt="Menu">
                <small class="text-center d-block text-gray-300 pt-2" id="menuuu">Sushi Roll</small>
            </div>
        </div>
    </div>






    <!-- tes tes -->
    <div data-aos="zoom-in-up">
        <section class="tstbite-components my-4 my-md-5 bekgrn rounded-6 shadow">
            <h2 class="py-3 mb-0  p-3 judul kuning stroke text-shadow" style="background: #EA2229">Popular Menu</h2>
            <div class="row p-3">
                <div class="col-lg-3 col-md-4 col-4">
                    <figure class="my-3 text-center tstbite-card">
                        <a href="category.html" class="tstbite-animation stretched-link rounded-circle">
                            <img src="assets/images/Teras_Menus/11.png" class="rounded-circle" alt="Menu">
                        </a>
                        <figcaption class="mt-2">
                            <a href="category.html" class="tstbite-category-title">Ramen Goreng Katsu</a>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-3 col-md-4 col-4">
                    <figure class="my-3 text-center tstbite-card">
                        <a href="category.html" class="tstbite-animation stretched-link rounded-circle">
                            <img src="assets/images/Teras_Menus/12.png" class="rounded-circle" alt="Menu">
                        </a>
                        <figcaption class="mt-2">
                            <a href="category.html" class="tstbite-category-title">Ramen Spicy Miruku</a>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-3 col-md-4 col-4">
                    <figure class="my-3 text-center tstbite-card">
                        <a href="category.html" class="tstbite-animation stretched-link rounded-circle">
                            <img src="assets/images/Teras_Menus/13.png" class="rounded-circle" alt="Menu">
                        </a>
                        <figcaption class="mt-2">
                            <a href="category.html" class="tstbite-category-title">Ramen Jamur Shitake</a>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-lg-3 col-md-4 col-4">
                    <figure class="my-3 text-center tstbite-card">
                        <a href="category.html" class="tstbite-animation stretched-link rounded-circle">
                            <img src="assets/images/Teras_Menus/14.png" class="rounded-circle" alt="Menu">
                        </a>
                        <figcaption class="mt-2">
                            <a href="category.html" class="tstbite-category-title">Sushi Dragon</a>
                        </figcaption>
                    </figure>
                </div>

            </div>
        </section>


    </div>
    <!-- tes tes -->



    <div data-aos="zoom-in-up">
        <div class="swiper-wrapper bekgrn p-3 pt-0 mb-3 shadow" >
            <div class="swiper-slide desk" >
                <div class="card border-0 text-black-90 bekgrn" >
                    <div class="row pt-md-5 mt-md-4">
                        <div class="col-lg-5 order-2 order-md-0" >
                            <img src="assets/images/terascafepalu_20210227_120005_0.jpg"  class="w-100 mb-3" alt="Foto Lokasi Teras Cafe">
                        </div>
                        <div class="col-lg-6 text-white p-3 bekgrn_merah"  id="desc" >
                            <h4 class="display-3 judul stroke text-shadow">Teras cafe Palu </h4>
                            <h6 class="display-6 judul kuning text-shadow">Spesialnya Mie Ramen</h6>
                            <p>Sejak tahun 2010 ramen kami sudah melayani masyarakat kota Palu sampai saat ini .<br>
                                <br>

                                Seiring berjalannya waktu kami akan selalu berinovasi & meningkatkan pelayanan kami untuk teras lovers semua.. <br>
                                <br>

                                Terima kasih telah menjadi bagian dari kami  <br>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="swiper-wrapper mt-3" data-aos="zoom-out">
            <div class="swiper-slide alamat shadow bekgrn" >
                <div class="card border-0 text-black-90 bekgrn_merah" >
                    <div class="row  mt-md-4">

                        <div class="col-lg-6 p-5 text-white" >
                            <h4 class="judul kuning text-shadow">Alamat </h4>

                            <p>Jl. Moh. Thamrin No.15  Besusu Tengah, Kec. Palu Timur. Kota Palu, Sulawesi Tengah 94111 </p>
                            <p>Nomor Telpon : 085241266600</p>
                            <p>Instagrm : @terascafepalu</p>

                        </div>
                        <div class="col-lg-6 order-2 order-md-0 " >
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.3312577396146!2d119.87705211411064!3d-0.8952779355755626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2d8bedd8f846f69b%3A0x6fc0336d0602740d!2sTeras%20Cafe%20Ramen%20Palu!5e0!3m2!1sid!2sid!4v1614262555550!5m2!1sid!2sid" width="200" height="250" style="border: 1px solid grey;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Comentt -->
    <div data-aos="zoom-in-up">
        <div class="judul pb-0">
            <div class="align-items-center" style="align-items: center">
                        <!-- <div class="col-md-6"> -->
                <center>
                        <figure class="text-center tstbite-profile rounded-6 border bekgrn_merah komen shadow">
                            <img src="assets/images/avatars/narutodd.jpg" class="rounded-circle" alt="gambar naruto uzumaki">
                            <figcaption class="bekgrn px-2 py-4 rounded-bottom-6">
                                <h6 class="font-weight-semibold inter-font f-size-24">Uzumaki Naruto</h6>
                                <p class="text-black mb-4">Ternyata jauh dari konoha masih ada nemu ramen yang enak.</p>
                            </figcaption>
                        </figure>
                </center>


                <center>
                    <!-- <div class="col-md-8"> -->
                    <figure class="text-center tstbite-profile rounded-6 border komen bekgrn_merah shadow">
                        <img src="assets/images/avatars/hinata.jpg" class="rounded-circle" alt="Gambar Hinata">
                        <figcaption class="bekgrn px-2 py-4 rounded-bottom-6">
                            <h6 class="font-weight-semibold inter-font f-size-24">Hyuga Hinata</h6>
                            <p class="text-black mb-4">Nemenin Naruto lagi misi, eh dapat ramen enak...</p>
                            <h6 class="font-weight-semibold inter-font f-size-24"></h6>
                            <p class="text-black mb-4"></p>
                        </figcaption>
                    </figure>
                </center>

            </div>

        </div>
    </div>
</div>



<footer class="tstbite-footer pt-3 pt-md-5 mt-5 bekgrn_merah">
    <div class="container">
        <div class="data" style="margin-left: -1rem;">
            <div id="opening"></div><h5 class="judul kuning strokek text-shadow">Silahkan Klaim Voucher Anda!</h5>
        </div>
        <div class="row" style="align-items: center; width: 99%;">
            <div class="col-lg-12" >
                <form class="mt-4" action="/inputcustomer" method="post">
                    @csrf
                    @method('post')
                    <div class="row">
                        <div class="col-sm-4" >
                            <div class="form-group custom-form-group">
                                <label style="color: black;">Nama Lengkap</label>
                                <div class="form-control-box">
                                    <input name="nama" type="text" class="form-control" >
                                    <span class="form-icon">
                                      <img src="assets/images/icons/user-plus.svg" alt="logo nama" width="24" height="24" viewBox="0 0 24 24">
                                        <rect data-name="Bounding Box" width="24" height="24" fill="rgba(255,255,255,0)"/>
                                        <path d="M16,19V17a3,3,0,0,0-3-3H5a3,3,0,0,0-3,3v2a1,1,0,0,1-2,0V17a5,5,0,0,1,5-5h8a5,5,0,0,1,5,5v2a1,1,0,0,1-2,0ZM4,5a5,5,0,1,1,5,5A5.006,5.006,0,0,1,4,5ZM6,5A3,3,0,1,0,9,2,3,3,0,0,0,6,5Z" transform="translate(3 2)" fill="#7f7f7f"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group custom-form-group">
                                <label style="color: black;">Nomor HP</label>
                                <div class="form-control-box">
                                    <input name="nope" type="text" class="form-control" value="+62">
                                    <span class="form-icon">
                                      <img alt="logo nomer hape" src="assets/images/icons/phone.svg" width="24" height="24" viewBox="0 0 24 24">
                                        <rect data-name="Bounding Box" width="24" height="24" fill="rgba(255,255,255,0)"/>
                                        <path d="M4.017,19.5A11,11,0,1,1,22,11v1a4,4,0,0,1-7.261,2.316A5,5,0,1,1,14,7V7a1,1,0,0,1,2,0v5a2,2,0,1,0,4,0V11a9,9,0,1,0-3.528,7.146,1,1,0,1,1,1.216,1.588A11,11,0,0,1,4.017,19.5ZM8,11a3,3,0,1,0,3-3A3,3,0,0,0,8,11Z" transform="translate(0.999 0.999)" fill="#7f7f7f"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group custom-form-group">
                                <label style="color: black;">Email</label>
                                <div class="form-control-box">
                                    <input name="email" type="text" class="form-control" >
                                    <span class="form-icon">
                                      <img src="assets/images/icons/send.svg" alt="logo email" width="24" height="24" viewBox="0 0 24 24">
                                        <rect data-name="Bounding Box" width="24" height="24" fill="rgba(255,255,255,0)"/>
                                        <path d="M3,18a3,3,0,0,1-3-3V3.01C0,3,0,2.99,0,2.98A3,3,0,0,1,3,0H19a3,3,0,0,1,3,2.968c0,.018,0,.036,0,.054V15a3,3,0,0,1-3,3ZM2,15a1,1,0,0,0,1,1H19a1,1,0,0,0,1-1V4.921l-8.427,5.9a1,1,0,0,1-1.147,0L2,4.921ZM11,8.78l8.895-6.226A1,1,0,0,0,19,2H3a1,1,0,0,0-.895.553Z" transform="translate(1 3)" fill="#7f7f7f"/>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input class="btn btn-light" type="submit" value="Kirim" style="width: 6rem; margin-left: 1rem;">
                </form>
            </div>

        </div>
        <div class="row pt-4 pb-0 pb-md-5"  style="border-top: solid 1px rgb(221, 221, 221);">
            <div class="col-md-6">
                <div class="tastebite-footer-contnet pe-0 pe-lg-5 me-0 me-md-5">
                    <a href="index.html">
                        <h5 class="judul kuning strokek text-shadow">Teras Cafe</h5>
                    </a>
                    <p class="mt-3 text-white pe-0 pe-lg-5 me-0 me-lg-4">"Specialnya Mie Ramen</p>
                </div>
            </div>
            <div class="col-md-2">
                <h6 class="caption font-weight-medium mb-2 inter-font">
                    <span>Teras Cafe</span>
                    <span class="d-inline-block d-md-none float-end">
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="8" viewBox="0 0 9.333 5.333">
                        <path d="M1.138.2A.667.667,0,0,0,.2,1.138l4,4a.667.667,0,0,0,.943,0l4-4A.667.667,0,1,0,8.2.2L4.667,3.724Z"/>
                      </svg>
                    </span>
                </h6>
                <ul>
                    <li><a class="text-white" href="#0">About us</a></li>
                    <li><a class="text-white" href="#0">Contact us</a></li>
                </ul>
            </div>

            <div class="col-md-2">
                <h6 class="caption font-weight-medium mb-2 inter-font">
                    <span>Follow</span>
                    <span class="d-inline-block d-md-none float-end">
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="8" viewBox="0 0 9.333 5.333">
                        <path d="M1.138.2A.667.667,0,0,0,.2,1.138l4,4a.667.667,0,0,0,.943,0l4-4A.667.667,0,1,0,8.2.2L4.667,3.724Z"/>
                      </svg>
                    </span>
                </h6>
                <ul>
                    <li><a  class="text-white" href="#0">Facebook</a></li>
                    <li><a  class="text-white" href="#0">Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
        <div class="row pb-4 pt-2 align-items-center mb-0">
            <div class="col-md-6 order-2 order-md-0">
                <p class="text-white small text-start mb-0">&copy; 2021 Rumah Cokor</p>
            </div>
            <div class="col-md-6">
                <div class="tstbite-social text-start text-md-end my-4 my-md-0">
                    <a href="https://www.facebook.com/terascafepalu">
                        <svg data-name="feather-icon/facebook" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                            <rect data-name="Bounding Box" width="20" height="20" fill="rgba(255,255,255,0)"/>
                            <path d="M6.667,18.333H3.333A.834.834,0,0,1,2.5,17.5V11.667H.833A.835.835,0,0,1,0,10.833V7.5a.834.834,0,0,1,.833-.833H2.5V5a5.006,5.006,0,0,1,5-5H10a.834.834,0,0,1,.833.833V4.167A.834.834,0,0,1,10,5H7.5V6.667H10A.833.833,0,0,1,10.808,7.7l-.833,3.334a.831.831,0,0,1-.809.631H7.5V17.5A.834.834,0,0,1,6.667,18.333Zm-5-10V10H3.333a.835.835,0,0,1,.834.833v5.834H5.833V10.833A.834.834,0,0,1,6.667,10h1.85l.416-1.667H6.667A.834.834,0,0,1,5.833,7.5V5A1.669,1.669,0,0,1,7.5,3.333H9.166V1.666H7.5A3.337,3.337,0,0,0,4.167,5V7.5a.835.835,0,0,1-.834.833Z" transform="translate(5 0.833)" fill="#ffffff"/>
                        </svg>
                    </a>
                    <a href="https://www.instagram.com/terascafepalu/">
                        <svg data-name="feather-icon/instagram" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                            <rect data-name="Bounding Box" width="20" height="20" fill="rgba(255,255,255,0)"/>
                            <path d="M5,18.333a5.005,5.005,0,0,1-5-5V5A5.006,5.006,0,0,1,5,0h8.333a5.005,5.005,0,0,1,5,5v8.333a5,5,0,0,1-5,5ZM1.667,5v8.333A3.337,3.337,0,0,0,5,16.667h8.333a3.337,3.337,0,0,0,3.333-3.333V5a3.337,3.337,0,0,0-3.333-3.334H5A3.338,3.338,0,0,0,1.667,5Zm4.59,7.076A4.164,4.164,0,1,1,9.2,13.3,4.161,4.161,0,0,1,6.256,12.076Zm.713-4.07a2.5,2.5,0,1,0,2.6-1.348A2.527,2.527,0,0,0,9.2,6.631,2.487,2.487,0,0,0,6.97,8.006Zm6.191-2.833a.833.833,0,1,1,.589.244A.834.834,0,0,1,13.161,5.173Z" transform="translate(0.833 0.833)" fill="#ffffff"/>
                        </svg>
                    </a>
                    <a href="https://twitter.com/yuqiarie">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20.004" height="20" viewBox="0 0 20.004 20">
                            <g data-name="feather-icon/twitter" transform="translate(0.002)">
                                <rect data-name="Bounding Box" width="20" height="20" fill="rgba(255,255,255,0)"/>
                                <path d="M6.894,16.644A13.387,13.387,0,0,1,.431,14.9a.834.834,0,0,1,.4-1.562H.869c.118,0,.237.007.354.007a8.925,8.925,0,0,0,3.708-.813,8.043,8.043,0,0,1-3.59-4.4A9.651,9.651,0,0,1,1.329,2.55a8.74,8.74,0,0,1,.412-1.214A.833.833,0,0,1,3.184,1.2,8.049,8.049,0,0,0,8.914,4.574l.255.023,0-.2A4.567,4.567,0,0,1,16.78,1.162a8.239,8.239,0,0,0,1.909-1,.827.827,0,0,1,.478-.155.852.852,0,0,1,.663.326.811.811,0,0,1,.149.707,7.28,7.28,0,0,1-1.662,3.145c.012.138.019.276.019.408a13.328,13.328,0,0,1-.922,4.987A11.157,11.157,0,0,1,6.894,16.644ZM2.839,3.378a7.847,7.847,0,0,0,.086,4.238,6.928,6.928,0,0,0,4.081,4.131.833.833,0,0,1,.13,1.451,10.505,10.505,0,0,1-3.025,1.414,10.962,10.962,0,0,0,2.786.367,9.566,9.566,0,0,0,6.869-2.807,10.5,10.5,0,0,0,2.9-7.576,2.817,2.817,0,0,0-.052-.538.834.834,0,0,1,.233-.75,5.6,5.6,0,0,0,.515-.583l-.285.1-.288.091a.831.831,0,0,1-.868-.25,2.9,2.9,0,0,0-5.088,1.953V5.45a.829.829,0,0,1-.812.833c-.084,0-.169,0-.253,0A9.659,9.659,0,0,1,6,5.525,9.669,9.669,0,0,1,2.839,3.378Z" transform="translate(-0.002 1.658)" fill="#ffffff"/>
                            </g>
                        </svg>
                    </a>
{{--                    <a href="#0">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="20.001" height="20" viewBox="0 0 20.001 20">--}}
{{--                            <g data-name="feather-icon/youtube" transform="translate(0)">--}}
{{--                                <rect data-name="Bounding Box" width="20" height="20" fill="rgba(255,255,255,0)"/>--}}
{{--                                <path d="M9.475,14.547,8.157,14.53c-.7-.013-1.348-.031-1.934-.053l-.592-.025a16.853,16.853,0,0,1-3.019-.316A3.189,3.189,0,0,1,.4,11.881,25.065,25.065,0,0,1,0,7.3,24.913,24.913,0,0,1,.408,2.681,3.168,3.168,0,0,1,2.618.411,17.815,17.815,0,0,1,5.8.089L6.887.049C7.536.029,8.205.016,8.876.008L9.8,0h.484L11.4.01c.584.007,1.173.02,1.748.036l.583.018a21.6,21.6,0,0,1,3.668.317A3.158,3.158,0,0,1,19.6,2.7,25.076,25.076,0,0,1,20,7.289a24.8,24.8,0,0,1-.408,4.58,3.164,3.164,0,0,1-2.209,2.269,16.78,16.78,0,0,1-3.014.315l-.592.025c-.586.023-1.237.041-1.934.053l-1.318.017ZM9.358,1.669c-.816.007-1.6.021-2.32.042l-1.109.04a18.192,18.192,0,0,0-2.868.266A1.468,1.468,0,0,0,2.037,3.031,23.455,23.455,0,0,0,1.667,7.3,23.669,23.669,0,0,0,2.018,11.5a1.488,1.488,0,0,0,1.031,1.024,18.758,18.758,0,0,0,2.977.273l.881.032c.374.011.793.022,1.282.031l1.3.017h1.026l1.3-.017c.488-.009.907-.019,1.282-.031l.881-.032.736-.035a14.14,14.14,0,0,0,2.228-.235,1.468,1.468,0,0,0,1.024-1.012,23.446,23.446,0,0,0,.37-4.232,23.255,23.255,0,0,0-.358-4.234,1.483,1.483,0,0,0-1.006-1.06,17.158,17.158,0,0,0-2.524-.232l-.776-.031c-.681-.023-1.453-.041-2.3-.053l-1.092-.009H9.8ZM7.545,10.616a.823.823,0,0,1-.254-.6V4.566a.835.835,0,0,1,.835-.834.822.822,0,0,1,.41.11l4.792,2.725a.833.833,0,0,1,0,1.449L8.537,10.74a.821.821,0,0,1-.411.111A.845.845,0,0,1,7.545,10.616ZM8.958,8.583l2.272-1.292L8.958,6Z" transform="translate(0 2.501)" fill="#7f7f7f"/>--}}
{{--                            </g>--}}
{{--                        </svg>--}}
{{--                    </a>--}}
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Tastebite Scripts -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/html5.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.mCustomScrollbar.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
<script src="{{asset('assets/js/hover-animation.min.js')}}"></script>
<script src="{{asset('assets/js/tastebite-scripts.js')}}"></script>
<script src="{{asset('assets/js/swiper-bundle.min.js')}}"></script>
<script src="{{asset('assets/js/masonry.min.js')}}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    AOS.init();
</script>
@if(session('tes'))
    <script>
        swal("Good job!", "Kamu sudah berhasil mengisi form", "success");
    </script>
@endif







</body>
</html>
