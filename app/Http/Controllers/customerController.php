<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\customer_user;

class customerController extends Controller
{
    public function customer(){

        $data = customer_user::paginate(5);
        return view('customer', ['data' => $data]);

    }

    public function input(Request $request){
        $data = new customer_user();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->save();

        return redirect('/')->with('tes','yes');

    }

    public function admin(){
        return view('adminlogin');
    }

    public function loginadmin(Request $request){
        if ($request->username == 'teras' && $request->password == 'ramen123'){
            return redirect('/customer');
        } else{
            return redirect()->back()->with('alert','gagal');
        }
    }

    public function searchcustomer(Request $request){
       $tes =  $request->nama;

       $has = customer_user::where('nama', 'LIKE',"%{$tes}%")->paginate();
        return view('customer', ['data' => $has]);

    }
}
