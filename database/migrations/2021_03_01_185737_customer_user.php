<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomerUser extends Migration
{
    public function up()
    {
        Schema::create('customer_user',function (Blueprint $table){
            $table->id();
            $table->text('nama');
            $table->text('email');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('customer_user');
    }
}
